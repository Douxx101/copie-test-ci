#!/bin/bash

# We need to install dependencies only for Docker
[[ ! -e /.dockerenv ]] && exit 0

set -xe

# Install git (the php image doesn't have it) which is required by composer
apt-get update -yqq
# apt-get install dialog apt-utils -y
apt-get install git -yqq

# Terminal sur non interactif
# echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections


# Install Apache/PHP/MySQL
apt-get install -y apache2
apt-get install php -y
apt-get install php-xml -y
apt-get install php-mbstring -y
apt-get install mariadb-server -y
apt-get install mariadb-client -y
# apt-get install pdo-mysql -y

# Install phpunit, the tool that we will use for testing
curl --location --output /usr/local/bin/phpunit https://phar.phpunit.de/phpunit.phar
chmod +x /usr/local/bin/phpunit

