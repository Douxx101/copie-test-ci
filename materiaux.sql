create database bdtest;
use bdtest;

CREATE TABLE materiaux (
  idProduit INT NOT NULL primary key,
  produit VARCHAR(255) NULL,
  quantite INT NULL,
  seuilMinimum INT NULL
);

--
-- Contenu de la table `materiaux`
--

INSERT INTO materiaux (idProduit, produit, quantite, seuilMinimum) VALUES
(1, 'Moteur', 0, 5);
INSERT INTO materiaux (idProduit, produit, quantite, seuilMinimum) VALUES
(2, 'Écrou', 356, 100);
INSERT INTO materiaux (idProduit, produit, quantite, seuilMinimum) VALUES
(3, 'Planche', 100, 50);
INSERT INTO materiaux (idProduit, produit, quantite, seuilMinimum) VALUES
(4, 'Vis', 500, 100);
INSERT INTO materiaux (idProduit, produit, quantite, seuilMinimum) VALUES
(5, 'Roue', 60, 30);
INSERT INTO materiaux (idProduit, produit, quantite, seuilMinimum) VALUES
(6, 'Boulons', 0, 150);

CREATE USER 'php'@'localhost' IDENTIFIED BY 'password';
GRANT SELECT ON bdtest.materiaux to 'php'@'localhost';
FLUSH PRIVILEGES;