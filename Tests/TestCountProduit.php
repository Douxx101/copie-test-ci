<?php

use PHPUnit\Framework\TestCase;

class TestCountProduit extends TestCase
{
    private $count;

    public function testService()
    {
        include_once 'Count.classe.php';
        $this->count = new Count();
        $this->assertEquals(6, $this->count->CountNumProduit());
    }

}