<?php

require_once "./Count.classe.php";

use PHPUnit\Framework\TestCase;

class TestCountProduitVide extends TestCase
{
    private $count;

    public function testService()
    {
        $this->count = new Count();
        $this->assertEquals(2, $this->count->CountNumProduitVide());
    }

}

