<?php
require_once './Connexion.classe.php';

class Count {
	public function CountNumProduit(){
		$monPDO = new Connexion();
		$monStatement = $monPDO->getPDO();
		$commande = $monStatement->prepare("SELECT COUNT(*) FROM materiaux");
		$commande->execute();
		$liste = $commande->fetch()[0];

		return $liste;
	}

    public function CountNumProduitVide(){
		$monPDO = new Connexion();
		$monStatement = $monPDO->getPDO();
		$commande = $monStatement->prepare("SELECT COUNT(*) FROM materiaux WHERE quantite = 0");
		$commande->execute();
		$liste = $commande->fetch()[0];

		return $liste;
	}

}
